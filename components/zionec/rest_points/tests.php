<?php


function callAPI($method, $url, $data, $apikey){

    $curl = curl_init();
    switch ($method){
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);
            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            break;
        case "DELETE":
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    // OPTIONS:
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'APIKEY: '.$apikey,
        'Content-Type: application/json',
    ));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

    // EXECUTE:
    $result = curl_exec($curl);
    if(!$result){die("Connection Failure");}
    curl_close($curl);
    return $result;
}

/*
 *
 * Вызов компонента
 * <?$APPLICATION->IncludeComponent(
    "zionec:rest_points",
    "",
    array(

    ),
    false
);?>
 *
 *
 *
 *
 * */



switch ($_GET["METHOD"])
{
    case "POST":
        $test = callAPI("POST", "http://azbooking.vilka.cc/test/", array("name"=>"testPost","adress"=>"testPost"), "KLHhflfWIEWPPLeo");//Добавление
        break;
    case "PUT":
        $test = callAPI("PUT", "http://azbooking.vilka.cc/test/", array("id"=>$_GET["ID"],"name"=>"testPost","adress"=>"testPost"), "KLHhflfWIEWPPLeo");//Обновление
        break;
    case "DELETE":
        $test = callAPI("DELETE", "http://azbooking.vilka.cc/test/", array("id"=>$_GET["ID"]), "KLHhflfWIEWPPLeo");//удаление
        break;
    default:
        $test = callAPI("", "http://azbooking.vilka.cc/test/", array(), "KLHhflfWIEWPPLeo");
//        $test = callAPI("POST", "http://azbooking.vilka.cc/test/", array("id"=>[идентификатор конкретной записи]), "KLHhflfWIEWPPLeo");

        break;
}

echo "<pre>".print_r($test, true)."</pre>";