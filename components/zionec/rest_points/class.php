<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @global CMain $APPLICATION */

use Bitrix\Main\Loader;

Loader::includeModule("highloadblock");

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;


class restPoint extends CBitrixComponent
{

    const
        tableID = 1,//ID таблицы
        testApikey = "KLHhflfWIEWPPLeo";
    public
        $HighloadEntity = false,
        $data = array(),
        $response = array();


    /*
     * Получение сущности HiloadBlocks
     * */
    public function getHighloadEntity()
    {
        $hlblock = HL\HighloadBlockTable::getById(self::tableID)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        $this->HighloadEntity = $entity_data_class;
    }

    /*
     * Получение данных либо весь список либо по ID
     *
     * */
    public function getPoint()
    {
        $params = array(
            "select" => array("*"),
            "order" => array("ID" => "ASC"),
        );
        $result = array();

        if($this->data["id"])
        {
           $params["filter"] = array("ID"=>$this->data["id"]);  // Задаем параметры фильтра выборки
        }

        $rsData = $this->HighloadEntity::getList($params);

        while($arData = $rsData->Fetch()){
            $item = array();

            foreach ($arData as $k=>$val)
            {
                $field = strtolower(str_ireplace("UF_", "", $k));
                if(is_object($val))
                {
                    $item[$field] = $val->format("d.m.Y H:i:s");
                }else{
                    $item[$field] = $val;

                }
            }

            $result[] = json_decode(json_encode($item, JSON_PRETTY_PRINT));


        }


        $this->response = $result;
//        $this->response["data"] = $this->data;

    }

    /*
     * Добавление точки
     * */
    public function addPoint()
    {

        $access = false;

        if(!$this->data["name"]) $this->response["error"]["name"] = "empty field";
        if(!$this->data["adress"]) $this->response["error"]["adress"] = "empty field";
        if(!$this->response["error"]) $access = true;

        if($access){

            $data = array(
                "UF_NAME"=>$this->data["name"],
                "UF_ADRESS"=>$this->data["adress"],
                "UF_CREATED_AT"=>date("d.m.Y H:i:s"),
                "UF_UPDATED_AT"=>date("d.m.Y H:i:s")
            );
            $result =  $this->HighloadEntity::add($data);


            if($result->isSuccess())
            {
                $this->response["message"] = "add elem successful";
            }else{
                $this->response["message"] = "unknown error";
            }

        }else{
            $result = false;
        }

    }

    /*
   * Удаление точки
   * */
    public function deletePoint()
    {

        $access = false;

        if(!$this->data["id"]) $this->response["error"]["id"] = "id not found";

        if(!$this->response["error"]) $access = true;

        if($access){

            $result =  $this->HighloadEntity::delete($this->data["id"]);

            if($result->isSuccess())
            {
                $this->response["message"] = "delete elem with id:".$this->data["id"]." successful";
            }else{
                $this->response["message"] = "unknown error";
            }

        }else{
            $result = false;
        }

    }

    /*
    * обновление точки
    * */
    public function updatePoint()
    {

        $access = false;

        if(!$this->data["id"]) $this->response["error"]["id"] = "id not found";
        if(!$this->data["name"]) $this->response["error"]["name"] = "empty field";
        if(!$this->data["adress"]) $this->response["error"]["adress"] = "empty field";

        if(!$this->response["error"]) $access = true;

        if($access){

            $data = array(
                "UF_NAME"=>$this->data["name"],
                "UF_ADRESS"=>$this->data["adress"],
                "UF_UPDATED_AT"=>date("d.m.Y H:i:s")
            );
            $result = $this->HighloadEntity::update($this->data["id"], $data);

            if($result->isSuccess())
            {
                $this->response["message"] = "update elem with id:".$this->data["id"]." successful";
            }else{
                $this->response["message"] = "unknown error";
            }

        }else{
            $result = false;
        }


    }


    public function apiAuth()
    {
        $headers = apache_request_headers();

        if(isset($headers['Apikey'])){
            $api_key = $headers['Apikey'];
            if($api_key != self::testApikey)
            {
                die("ACCESS DENIED");
            }
        }else{
            die("ACCESS DENIED");
        }
    }

    public function executeComponent()
    {

        $this->apiAuth();//фиктивная проверка на apikey в заголовке
        $this->getHighloadEntity();//получение сущности Highload block а

        $inputData = file_get_contents("php://input");
        parse_str($inputData,$this->data);



        switch ($this->request->getRequestMethod())
        {
            case "PUT"://обновление
                $this->updatePoint();
                break;
            case "POST"://добавление
                $this->addPoint();
            break;
            case "DELETE"://удаление
                $this->deletePoint();
                break;
            default://получение данных

                if(empty($this->data))
                {
                    $this->data["id"] = $this->request->get("id");
                }

                $this->getPoint();
                break;
        }


        $this->arResult = json_encode($this->response, JSON_PRETTY_PRINT);


        $this->includeComponentTemplate();
    }

}